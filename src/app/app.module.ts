import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ListThingsComponent } from './list-things/list-things.component';
import { DetailThingComponent } from './detail-thing/detail-thing.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListThingsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ListThingsComponent,
    DetailThingComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
